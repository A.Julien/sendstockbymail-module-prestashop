<?php

if (!defined('_PS_VERSION_'))
  exit;
 
class sendstockbymail extends Module {

	public function __construct() {
	    $this->name = 'sendstockbymail';
	    $this->tab = 'emailing';
	    $this->version = '1.0.0';
	    $this->author = 'Julien Alauzet';
	    $this->need_instance = 0;
	    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
	    $this->bootstrap = true;
	 
	    parent::__construct();
	 
	    $this->displayName = $this->l('Send stock by Email');
	    $this->description = $this->l('After each update, the stock is send by email');
	 
	    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	 
	    if (!Configuration::get('sendstockbymail'))      
	     	$this->warning = $this->l('No name provided');
	}


/* ---- INSTALLATION DU MODULE ---- */
	public function install() {
	  	if (Shop::isFeatureActive())
	    	Shop::setContext(Shop::CONTEXT_ALL);
	 
	  	if (!parent::install() ||
	    	!$this->registerHook('actionUpdateQuantity') ||
	    	!Configuration::updateValue('sendstockbymail', '')
	  	)
	    return false;
	 
	 return true;
	}


/* ---- DESINSTALLATION DU MODULE ---- */
	public function uninstall() {
	  	if (!parent::uninstall() ||
	    	!Configuration::deleteByName('sendstockbymail_NAME')
	  	)
	    return false;
	 
	 return true;
	}


/* ---- INFORME DE LA MISE A JOUR DES STOCK ---- */
	public function hookActionUpdateQuantity($params){

	/* -- Requete pour recuprer le Nom et la quantité du produit --*/
	$result = Db::getInstance()->getRow(
		"SELECT `ps_product_lang`.`name`,`ps_stock_available`.`quantity` FROM `ps_stock_available`
		INNER JOIN`ps_product` ON `ps_stock_available`.`id_product`= `ps_product`.`id_product`
		INNER JOIN `ps_product_lang` ON `ps_product`.`id_product`= `ps_product_lang`.`id_product`
		WHERE `id_product_attribute` = '".$params['id_product_attribute']."'"
	);
	/* -- Requete pour recuprer les attributs du produit --*/
	$attributes	= Db::getInstance()->executeS(
		"SELECT `ps_attribute_lang`.`name` FROM `ps_attribute_lang`
		WHERE `id_attribute` IN (
			SELECT `id_attribute` FROM `ps_product_attribute_combination`
			WHERE `ps_product_attribute_combination`.`id_product_attribute` = '".$params['id_product_attribute']."'
		)"
	);	
	
	/* -- Préparation des variables pour l'email --*/
	global $cookie;
	$subject = 'Quantité de '.$result['name'].'.';
	$data = array(
		'{product_name}' => strval($result['name']) ,
		'{product_quantity}' => intval($result['quantity'])  ,
		'{product_attr_1}' => strval($attributes[0]['name']),
		'{product_attr_2}' => strval($attributes[1]['name']),
	);
	$to = 'alauzetj.dev@gmail.com';
	
	/* -- Envoi de l'email --*/
		Mail::Send(intval($cookie->id_lang),
			'emailStock',
			$subject ,
			$data,
			$to,
			'Alauzet Julien',
			'alauzetj.dev@gmail.com',
			'DFT',
			array(),
			true,
			_PS_MAIL_DIR_,
			false,
			''
		);
	}

}


		
